#version 330         

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 texCoords;
layout (location = 3) in vec3 tangent;
layout (location = 4) in vec3 bitangent;
layout (location = 5) in ivec4 BoneIDs;
layout (location = 6) in vec4 Weights;
														
const int MAX_BONES = 100;

uniform mat4 gBones[MAX_BONES];

uniform mat4 modelMat;
uniform mat4 viewMat;
uniform mat4 projectionMat;

out VS_OUT {
	vec3 WorldPos;
	vec3 Normal;
	vec2 TexCoords;
	vec3 TangentLightPos;
	vec3 TangentViewPos;
	vec3 TangentFragPos;
} vs_out;

void main()
{       
	mat4 MVP = projectionMat * viewMat * modelMat;
	mat4 I = mat4(1,0,0,0,  0,1,0,0,  0,0,1,0,  0,0,0,1);

	mat4 BoneTransform = gBones[BoneIDs[0]] * Weights[0];
	BoneTransform     += gBones[BoneIDs[1]] * Weights[1];
	BoneTransform     += gBones[BoneIDs[2]] * Weights[2];
	BoneTransform     += gBones[BoneIDs[3]] * Weights[3];

//	BoneTransform = BoneTransform / 4.0;

	vec4 PosL          = BoneTransform * vec4(position, 1.0);
	gl_Position        = projectionMat * viewMat * modelMat * PosL;

	vs_out.TexCoords   = texCoords;

	vs_out.WorldPos     = (modelMat * PosL).xyz;                                
	vec4 NormalL		= BoneTransform * vec4(normal, 1.0);
	vs_out.Normal		= (modelMat * NormalL).xyz;
	//From normal mapping shader
/*    
	mat3 normalMatrix = transpose(inverse(mat3(modelMat)));
	

	vec3 T = normalize(normalMatrix * tangent);
	vec3 B = normalize(normalMatrix * bitangent);
	vec3 N = normalize(normalMatrix * normal);
	//Get inverse of TBN matrix to transform vectors into tangent space
	//Transpose of an orthgonal matrix is equal to its inverse and cheaper to compute
	mat3 TBN = transpose(mat3(T, B, N));
	
	//Transforming all of these into tangent space rather than normals to world
	//space should end up faster because they do not change per-fragment
	vs_out.TangentLightPos = TBN * lightPos;
	vs_out.TangentViewPos  = TBN * viewPos;
	//Exact positiion can be interpolated
	vs_out.TangentFragPos  = TBN * vs_out.TangentFragPos;
*/
}
