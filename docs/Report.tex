\documentclass{sig-alternate-05-2015} % V1.2
%%\usepackage[]{minted} 	%Code highlighting
\usepackage{gensymb}	%Degree symbol
\usepackage{listings}	%Alternate code listings#
\usepackage{color}
\usepackage{float}		%Better figure placement
% \usepackage{tcolorbox}
% \usepackage{etoolbox}
% \BeforeBeginEnvironment{minted}{\begin{tcolorbox}}%
% \AfterEndEnvironment{minted}{\end{tcolorbox}}%
\lstset{language=C++}
\lstdefinestyle{customc}{
  belowcaptionskip=1\baselineskip,
  breaklines=true,
  xleftmargin=\parindent,
  language=C++,
  showstringspaces=false,
  basicstyle=\footnotesize\ttfamily,
  keywordstyle=\bfseries\color{blue}\ttfamily,
  commentstyle=\color{green}\ttfamily,
  %identifierstyle=\color{blue},
  stringstyle=\color{red},
  tabsize=2
}

\doi{10.1145/1559755.1559763}

%ISBN
\isbn{123-4567-24-567/08/06}

% Copyright
%\setcopyright{acmcopyright}

\begin{document}

% Page heads

% Title portion
\title{CS7033: Real-Time Animation Lab3 - Hand Animation}

\numberofauthors{1}
\author{
	\alignauthor
		Daniel Walsh\\
	 	\affaddr{Trinity College Dublin}\\
		\email{walshd15@tcd.ie}
}

\maketitle


\section{Introduction}
\begin{figure}[ht]
\minipage {0.15\textwidth}
\includegraphics[width=\linewidth]{animation}
\endminipage \hfill
\minipage {0.15\textwidth}
\includegraphics[width=\linewidth]{animation1}
\endminipage \hfill
\minipage {0.15\textwidth}
\includegraphics[width=\linewidth]{animation2}
\endminipage \hfill
\caption{Three stages of the animation.}
\end{figure}


My submission for this lab  displays a disembodied hand and allows a user to click to play a hand closing animation. There is also a debug mode in which the skin is not drawn but only the bones/joints. The importer code for this program is based on Tutorial 38 from http://ogldev.atspace.co.uk \cite{ogldev}.

\section{Implementation Summary}

\subsection{Bone Class}

\begin{lstlisting}[frame=single, caption=Bone Class, style=customc] 
struct Bone
{
  glm::mat4 BoneOffset;
  glm::mat4 FinalTransformation;        
  //Intermediate result for debug output
  glm::mat4 localTransform; 

  std::string name;
	
  Bone()
  {
    BoneOffset = glm::mat4(0);
    FinalTransformation = glm::mat4(0);            
  }

  std::vector<Bone*> children;
};
\end{lstlisting}

The bone class was implemented as a struct containing the transformation matrices needed, the name of the bone, and a vector of pointers to its children.

The bone offset matrix defines the transformation from model space into this bone's local space at rest.

The final transformation matrix stores the total transformation of the bone for this animation frame.

The local transform matrix is an intermediate result defining the total transformation relative to the bone's parent, which is only used for debugging output when drawing the bones 
and joints. This matrix could have been recalculated as needed but this solution seemed cleaner.

The vector of pointers to children is used to traverse the bone hierarchy to calculate the FinalTransformation matrices.

\subsection{Skeleton Class}

\begin{figure}[h]
\centering
\includegraphics[width=7cm]{bone_view}
\caption{Bone view of the animation}
\label{fig:skeleton}
\end{figure}

\begin{lstlisting}[frame=single, caption=Skeleton Class, style=customc] 
  Bone *rootBone;
  std::vector<Bone> allBones;

  void ReadBoneHeirarchy(float AnimationTime, Bone* bone, const glm::mat4 ParentTransform);
\end{lstlisting}

I did not implement an individual skeleton class but instead implemented the functionality in the existing model class which now contains a pointer to the rootBone and a vector containing all of the bones in the model.

For debugging all of the bones and joints may be drawn to the screen as shown in Figure~\ref{fig:skeleton}


The bone transformations are calculated in the ReadBoneHeirarchy function. An abridged version of this function is given in Listing~\ref{func:bone_hierarchy}

\begin{lstlisting}[frame=single, caption=ReadBoneHeirarchy Function, label=func:bone_hierarchy, style=customc] 

void Model::ReadBoneHeirarchy(float AnimationTime, Bone* bone, const glm::mat4 ParentTransform)
{
const aiAnimation* pAnimation = animations[0];

glm::mat4 NodeTransformation = glm::mat4(1);

const aiNodeAnim* pNodeAnim = FindNodeAnim(pAnimation, bone->name);

if (pNodeAnim) {
	aiVector3D Scaling;
	glm::mat4 ScalingM = CalcInterpolatedScaling(Scaling, AnimationTime, pNodeAnim);
	
	aiQuaternion RotationQ;
	CalcInterpolatedRotation(RotationQ, AnimationTime, pNodeAnim);        
	glm::mat4 RotationM = glm::mat4(1);

	aiVector3D Translation;
	CalcInterpolatedPosition(Translation, AnimationTime, pNodeAnim);

	// Combine the above transformations
	NodeTransformation = TranslationM * RotationM * ScalingM;
}

glm::mat4 GlobalTransformation = ParentTransform * NodeTransformation;

bone->localTransform = GlobalTransformation;

if (m_BoneMapping.find(bone->name) != m_BoneMapping.end()) 
{
	int BoneIndex = m_BoneMapping[bone->name];

	bone->FinalTransformation = inverseTransform * GlobalTransformation * bone->BoneOffset;
}

for (int i = 0 ; i < bone->children.size() ; i++) {
	ReadBoneHeirarchy(AnimationTime, bone->children[i], GlobalTransformation);
}
} 


\end{lstlisting}


\subsection{Rendering}

For rendering, an overloaded draw function was written that takes the current time as an arguement, calculates the transformation for each bone and passes these to the shader. The object is then drawn as normal.

\begin{lstlisting}[frame=single, caption=Draw Method, style=customc] 
void Model::Draw(glm::mat4 &v, glm::mat4 &p, float time)
{
  std::vector<glm::mat4> transforms;
  this->BoneTransform(time, transforms);

  for (int i=0;i<transforms.size();i++) 
  {
    std::ostringstream uniform;
    uniform << "gBones[" << i << "]";
		
    this->shader->setUniformMatrix4fv(uniform.str(), transforms[i]);
  }

  this->Draw(v, p);
}
\end{lstlisting}

These matrices are then used in the vertex shader to transform the drawn geometry.
\begin{lstlisting}[frame=single, caption=vertex shader transform, style=customc] 
layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 texCoords;
layout (location = 3) in vec3 tangent;
layout (location = 4) in vec3 bitangent;
layout (location = 5) in ivec4 BoneIDs;
layout (location = 6) in vec4 Weights;

void main()
{      
	mat4 BoneTransform = 
		gBones[BoneIDs[0]] * Weights[0];
	BoneTransform     
		+= gBones[BoneIDs[1]] * Weights[1];
	BoneTransform     
		+= gBones[BoneIDs[2]] * Weights[2];
	BoneTransform     
		+= gBones[BoneIDs[3]] * Weights[3];

	vec4 PosL = 
		BoneTransform * vec4(position, 1.0);

	gl_Position = 
		projectionMat * viewMat * modelMat * PosL;
}
\end{lstlisting}


% Bibliography
\bibliographystyle{abbrv}
\bibliography{references}
\balancecolumns
\end{document}
