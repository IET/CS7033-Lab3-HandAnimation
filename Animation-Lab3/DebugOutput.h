#include <GL/glew.h>
#include <glm/glm.hpp>
#include "ShaderManager.hpp"
#include <vector>

#define MAX_LINES 50

struct DebugSphere{
	GLuint VBO, VAO;
	GLfloat sphereVertices[72];
};

struct DebugLine{
	GLuint VBO, VAO;
	GLfloat vertices[MAX_LINES * 3];
};


static class DebugOutput {
public:
	static DebugSphere	debugSphere;
	static DebugLine	debugLine;
	
	static void Init();
	static void DrawSphere(glm::mat4 modelMatrix, glm::mat4 v, glm::mat4 p, glm::vec3 pos, float radius);
	static void DrawLine(glm::mat4 modelMatrix, glm::mat4 v, glm::mat4 p, glm::vec3 start, glm::vec3 end);
	static void DrawLine(glm::mat4 modelMatrix, glm::mat4 v, glm::mat4 p, std::vector<glm::vec3> startPoints, std::vector<glm::vec3> endPoints);

	static Shader* shader;
};