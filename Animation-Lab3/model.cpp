#include "model.h"
#include <assimp/matrix3x3.h>
#include "TextureUtils.h"
#include "DebugOutput.h"


/*  Functions   */
//Default constructor
Model::Model() 
{
	location = glm::vec3(0, 0, 0);
	quaternionOrientaton = glm::quat();
	numVerts = 0;
	numBones = 0;
	rootBone = nullptr;
	//path = "";

	this->modelMatrix = glm::mat4();
	ilInit();
}

// Constructor, expects a filepath to a 3D model.
Model::Model(string const & path, bool gamma) : gammaCorrection(gamma)
{
	location = glm::vec3(0, 0, 0);
	quaternionOrientaton = glm::quat();
	numVerts = 0;
	numBones = 0;
	rootBone = nullptr;
	//this->path = path;

	this->modelMatrix = glm::mat4();
	this->loadModel(path);
	ilInit();
}

//TODO - Get copy constructor and = operator to work correctly
// Copy constructor
Model::Model(const Model& other)
	:location(other.location), quaternionOrientaton(other.quaternionOrientaton), numVerts(other.numVerts), numBones(other.numBones)
{
	this->location = other.location;
	this->quaternionOrientaton = glm::quat();
}

Model Model::operator=(const Model &other )
{ 
	if (this == &other)
		return *this;

	this->location = other.location;
	this->quaternionOrientaton = glm::quat();

	return *this;
}

void Model::Draw()
{
	if (this->shader == NULL)
	{
		//Spam console output...
		cout << "Cannot draw model without shader";
		return;
	}
	//this->translate(location);

	//this->rotate(rotation);

	this->shader->setUniformMatrix4fv("modelMat", this->modelMatrix);
	for(GLuint i = 0; i < this->meshes.size(); i++)
		this->meshes[i].Draw(this->shader);

	this->modelMatrix = glm::mat4();
}

// Draws the model, and thus all its meshes
void Model::Draw(glm::mat4 &v, glm::mat4 &p)
{
	if (this->shader == NULL)
	{
		//Spam console output...
		cout << "Cannot draw model without shader";
		return;
	}

	this->shader->setUniformMatrix4fv("modelMat", this->modelMatrix);
	this->shader->setUniformMatrix4fv("viewMat", v);
	this->shader->setUniformMatrix4fv("projectionMat", p);
	for(GLuint i = 0; i < this->meshes.size(); i++)
		this->meshes[i].Draw(this->shader);

	this->shader->disableShader();
}

// Draws the model, and applies animation based on time input
void Model::Draw(glm::mat4 &v, glm::mat4 &p, float time)
{
	std::vector<glm::mat4> transforms;
	this->BoneTransform(time, transforms);

	for (int i = 0 ; i < transforms.size() ; i++) 
	{
		std::ostringstream uniform;
		uniform << "gBones[" << i << "]";
		
		this->shader->setUniformMatrix4fv(uniform.str(), transforms[i]);
	}

	this->Draw(v, p);
}

void Model::DrawJoints(glm::mat4 &v, glm::mat4 &p)
{
	for (int i =0; i < allBones.size(); i ++)
	{
		//DebugOutput::DrawSphere(this->modelMatrix * glm::inverse(inverseTransform) * bone->FinalTransformation * glm::inverse(bone->BoneOffset), v, p, glm::vec3(0), 0.2f);
		DebugOutput::DrawSphere(this->modelMatrix * allBones[i].localTransform, v, p, glm::vec3(0), 0.02f); //I just saved an intermediate result, bit cleaner than the alternative (above)
	}
}

void Model::DrawBones(glm::mat4 &v, glm::mat4 &p)
{
	DrawBones(v, p, rootBone);
}

void Model::DrawBones(glm::mat4 &v, glm::mat4 &p, Bone* bone)
{
	for (int i = 0; i < bone->children.size(); i++)
	{
		DebugOutput::DrawLine(this->modelMatrix, v, p, glm::vec3(bone->localTransform[3]), glm::vec3(bone->children[i]->localTransform[3]));
		DrawBones(v, p, bone->children[i]);
	}
}

Shader* Model::getShader() {
	return this->shader;
}

glm::mat4 Model::getModelMatrix() {
	return this->modelMatrix;
}

glm::vec3 Model::getLocation()
{
	return this->location;
}

void Model::setShader(Shader* s) {
	this->shader = s;
}

void Model::setLocation (glm::vec3 newPos) {
	location = newPos;
}

void Model::moveBy(glm::vec3 move) {
	location.x += move.x;
	location.y += move.y;
	location.z += move.z;
}

void Model::setRotation(glm::vec3 newRot) 
{
	this->modelMatrix = glm::mat4(1);
	quaternionOrientaton = glm::angleAxis(glm::length(newRot), glm::normalize(newRot));
	this->modelMatrix *= glm::mat4_cast(glm::angleAxis(glm::length(newRot), glm::normalize(newRot)));
}
	
void Model::rotateBy(glm::vec3 rot) 
{
	quaternionOrientaton *= glm::angleAxis(glm::length(rot), glm::normalize(rot));
	//glm::mat4 rotMat = glm::mat4_cast(quaternionOrientaton);
	this->modelMatrix *= glm::mat4_cast(glm::angleAxis(glm::length(rot), glm::normalize(rot)));

	//Ensure location doesn't change (means I don't have to worry about order of operations)
	this->modelMatrix[3] = glm::vec4(location.x, location.y, location.z, 1.0f);
}

void Model::translate(glm::vec3 t)
{
	this->modelMatrix = glm::translate(this->modelMatrix, t);
}

void Model::scale(glm::vec3 s)
{
	this->modelMatrix = glm::scale(this->modelMatrix, s);
}
	
void Model::load(string path)
{
	this->loadModel(path);
}

//TODO - Create some way to switch animations
void Model::BoneTransform(float time, vector<glm::mat4> &transforms)
{
	transforms.resize(numBones);

	if (animations.size() == 0)
	{
		//This is not an animated mesh
		return;
	}

	float ticksPerSecond = (float)(animations[0]->mTicksPerSecond != 0 ? animations[0]->mTicksPerSecond : 25.0f);
	float TimeInTicks = time * ticksPerSecond;
	float AnimationTime = fmod(TimeInTicks, (float)animations[0]->mDuration);

	ReadBoneHeirarchy(AnimationTime, rootBone, glm::mat4(1));
	
	for (int i = 0 ; i < numBones ; i++) {
		transforms[i] = allBones[i].FinalTransformation;
	}
}

void Model::ReadBoneHeirarchy(float AnimationTime, Bone* bone, const glm::mat4 ParentTransform)
{
	const aiAnimation* pAnimation = animations[0];

	glm::mat4 NodeTransformation = glm::mat4(1);

	const aiNodeAnim* pNodeAnim = FindNodeAnim(pAnimation, bone->name);

	if (pNodeAnim) {
		// Interpolate scaling and generate scaling transformation matrix
		aiVector3D Scaling;
		CalcInterpolatedScaling(Scaling, AnimationTime, pNodeAnim);
		glm::mat4 ScalingM = glm::mat4(1);
		ScalingM[0][0] = Scaling.x;
		ScalingM[1][1] = Scaling.y;
		ScalingM[2][2] = Scaling.z;

		// Interpolate rotation and generate rotation transformation matrix
		aiQuaternion RotationQ;
		CalcInterpolatedRotation(RotationQ, AnimationTime, pNodeAnim);        
		glm::mat4 RotationM = glm::mat4(1);
		aiMatrix3x3t<float> assimpRotationMatrix = RotationQ.GetMatrix();

		for (int i =0; i < 3; i++)
			for (int j =0; j < 3; j++)
				RotationM[i][j] = assimpRotationMatrix[j][i];

		// Interpolate translation and generate translation transformation matrix
		aiVector3D Translation;
		CalcInterpolatedPosition(Translation, AnimationTime, pNodeAnim);
		glm::mat4 TranslationM = glm::mat4(1);
		TranslationM[3][0] = Translation.x;
		TranslationM[3][1] = Translation.y;
		TranslationM[3][2] = Translation.z;

		// Combine the above transformations
		NodeTransformation = TranslationM * RotationM * ScalingM;
	}

	glm::mat4 GlobalTransformation = ParentTransform * NodeTransformation;
	bone->localTransform = GlobalTransformation;
	
	if (m_BoneMapping.find(bone->name) != m_BoneMapping.end()) 
	{
		int BoneIndex = m_BoneMapping[bone->name];
		bone->FinalTransformation = inverseTransform * GlobalTransformation * bone->BoneOffset;
	}
	
	for (unsigned int i = 0 ; i < bone->children.size() ; i++) {
		ReadBoneHeirarchy(AnimationTime, bone->children[i], GlobalTransformation);
	}
} 

const aiNodeAnim* Model::FindNodeAnim(const aiAnimation* pAnimation, const string NodeName)
{
	for (int i = 0 ; i < pAnimation->mNumChannels ; i++) {
		const aiNodeAnim* pNodeAnim = pAnimation->mChannels[i];
		
		if (string(pNodeAnim->mNodeName.data) == NodeName) {
			return pNodeAnim;
		}
	}
	
	return nullptr;
}

void Model::CalcInterpolatedPosition(aiVector3D& Out, float AnimationTime, const aiNodeAnim* pNodeAnim)
{

	if (pNodeAnim->mNumPositionKeys == 1) {
		Out = pNodeAnim->mPositionKeys[0].mValue;
		return;
	}

	unsigned int PositionIndex = FindPosition(AnimationTime, pNodeAnim);
	unsigned int NextPositionIndex = (PositionIndex + 1);
	assert(NextPositionIndex < pNodeAnim->mNumPositionKeys);
	float DeltaTime = (float)(pNodeAnim->mPositionKeys[NextPositionIndex].mTime - pNodeAnim->mPositionKeys[PositionIndex].mTime);
	float Factor = (AnimationTime - (float)pNodeAnim->mPositionKeys[PositionIndex].mTime) / DeltaTime;
	assert(Factor >= 0.0f && Factor <= 1.0f);
	const aiVector3D& Start = pNodeAnim->mPositionKeys[PositionIndex].mValue;
	const aiVector3D& End = pNodeAnim->mPositionKeys[NextPositionIndex].mValue;
	aiVector3D Delta = End - Start;
	Out = Start + Factor * Delta;
}


void Model::CalcInterpolatedRotation(aiQuaternion& Out, float AnimationTime, const aiNodeAnim* pNodeAnim)
{
	// we need at least two values to interpolate...
	if (pNodeAnim->mNumRotationKeys == 1) {
		Out = pNodeAnim->mRotationKeys[0].mValue;
		return;
	}

	unsigned int RotationIndex = FindRotation(AnimationTime, pNodeAnim);
	unsigned int NextRotationIndex = (RotationIndex + 1);
	assert(NextRotationIndex < pNodeAnim->mNumRotationKeys);
	float DeltaTime = (float)(pNodeAnim->mRotationKeys[NextRotationIndex].mTime - pNodeAnim->mRotationKeys[RotationIndex].mTime);
	float Factor = (AnimationTime - (float)pNodeAnim->mRotationKeys[RotationIndex].mTime) / DeltaTime;
	assert(Factor >= 0.0f && Factor <= 1.0f);
	const aiQuaternion& StartRotationQ = pNodeAnim->mRotationKeys[RotationIndex].mValue;
	const aiQuaternion& EndRotationQ   = pNodeAnim->mRotationKeys[NextRotationIndex].mValue;    
	aiQuaternion::Interpolate(Out, StartRotationQ, EndRotationQ, Factor);
	Out = Out.Normalize();
}


void Model::CalcInterpolatedScaling(aiVector3D& Out, float AnimationTime, const aiNodeAnim* pNodeAnim)
{
	if (pNodeAnim->mNumScalingKeys == 1) {
		Out = pNodeAnim->mScalingKeys[0].mValue;
		return;
	}

	unsigned int ScalingIndex = FindScaling(AnimationTime, pNodeAnim);
	unsigned int NextScalingIndex = (ScalingIndex + 1);
	assert(NextScalingIndex < pNodeAnim->mNumScalingKeys);
	float DeltaTime = (float)(pNodeAnim->mScalingKeys[NextScalingIndex].mTime - pNodeAnim->mScalingKeys[ScalingIndex].mTime);
	float Factor = (AnimationTime - (float)pNodeAnim->mScalingKeys[ScalingIndex].mTime) / DeltaTime;
	assert(Factor >= 0.0f && Factor <= 1.0f);
	const aiVector3D& Start = pNodeAnim->mScalingKeys[ScalingIndex].mValue;
	const aiVector3D& End   = pNodeAnim->mScalingKeys[NextScalingIndex].mValue;
	aiVector3D Delta = End - Start;
	Out = Start + Factor * Delta;
} 
		
unsigned int Model::FindPosition(float AnimationTime, const aiNodeAnim* pNodeAnim)
{

	for (int i = 0 ; i < pNodeAnim->mNumPositionKeys - 1 ; i++) {
		if (AnimationTime < (float)pNodeAnim->mPositionKeys[i + 1].mTime) {
			return i;
		}
	}

	assert(0);

	return 0;
}

unsigned int Model::FindRotation(float AnimationTime, const aiNodeAnim* pNodeAnim)
{
	assert(pNodeAnim->mNumRotationKeys > 0);

	for (int i = 0 ; i < pNodeAnim->mNumRotationKeys - 1 ; i++) {
		if (AnimationTime < (float)pNodeAnim->mRotationKeys[i + 1].mTime) {
			return i;
		}
	}

	assert(0);

	return 0;
}

unsigned int Model::FindScaling(float AnimationTime, const aiNodeAnim* pNodeAnim)
{
	assert(pNodeAnim->mNumScalingKeys > 0);

	for (int i = 0 ; i < pNodeAnim->mNumScalingKeys - 1 ; i++) {
		if (AnimationTime < (float)pNodeAnim->mScalingKeys[i + 1].mTime) {
			return i;
		}
	}

	assert(0);

	return 0;
} 

Model::~Model() {
	//delete[] modelMatrices;
}

//----------------------------------------------------------------------------------------------------------------
//-------------Private--------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------

/*  Functions   */
// Loads a model with supported ASSIMP extensions from file and stores the resulting meshes in the meshes std::vector.
void Model::loadModel(std::string path)
{
	minBounds = glm::vec3(100000000.0f, 100000000.0f, 100000000.0f);
	maxBounds = glm::vec3(-100000000.0f, -100000000.0f, -100000000.0f);
		
	// Read file via ASSIMP
	//When scene is initialised, it doesn't own the data, importer does so it can't go out of scope (unless I manage to make a deep copy)
	//Assimp::Importer importer;

	//const aiScene* scene = importer.ReadFile(path, aiProcess_Triangulate | aiProcess_FlipUVs | aiProcess_CalcTangentSpace);
	const aiScene* scene = importer.ReadFile(path, aiProcess_Triangulate | aiProcess_GenSmoothNormals | aiProcess_FlipUVs | aiProcess_CalcTangentSpace);

	// Check for errors
	if(!scene || scene->mFlags == AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode) // if is Not Zero
	{
		std::cout << "ERROR::ASSIMP:: " << importer.GetErrorString() << endl;
		return;
	}

	//Load root node inverse transform
	for (int i =0; i < 4; i++)
		for (int j =0; j < 4; j++)
			inverseTransform[i][j] = scene->mRootNode->mTransformation[i][j];
	inverseTransform = glm::inverse(inverseTransform);

	//Load animations
	for (int i=0; i < scene->mNumAnimations; i++)
		animations.push_back(scene->mAnimations[i]);

	// Retrieve the directory path of the filepath
	this->directory = path.substr(0, path.find_last_of('/'));

	// Process ASSIMP's root node recursively
	this->processNode(scene->mRootNode, scene);
	
	//TODO - Build node hierarchy at this point. Bone map will be created so it'll just be another O(n) traversal to construct a tree of bones
	BuildBoneHierarchy(scene->mRootNode);
}

// Processes a node in a recursive fashion. Processes each individual mesh located at the node and repeats this process on its children nodes (if any).
void Model::processNode(aiNode* node, const aiScene* scene)
{
	// Process each mesh located at the current node
	for(GLuint i = 0; i < node->mNumMeshes; i++)
	{
		// The node object only contains indices to index the actual objects in the scene. 
		// The scene contains all the data, node is just to keep stuff organized (like relations between nodes).
		aiMesh* mesh = scene->mMeshes[node->mMeshes[i]]; 
		this->meshes.push_back(this->processMesh(mesh, scene));			
	}
	// After we've processed all of the meshes (if any) we then recursively process each of the children nodes
	for(GLuint i = 0; i < node->mNumChildren; i++)
	{
		this->processNode(node->mChildren[i], scene);
	}
}

void Model::BuildBoneHierarchy(aiNode* node)
{
	string BoneName(node->mName.data);

	//If this is a bone in the hierarchy (not all Assimp nodes are)
	if (m_BoneMapping.find(BoneName) != m_BoneMapping.end()) 
	{
		if (rootBone == nullptr)	
		{
			rootBone = &(allBones[m_BoneMapping[BoneName]]);
			//BuildHierarchy(node, rootBone);
		}

		for (int i = 0; i < node->mNumChildren; i++)
		{
			if (m_BoneMapping.find(node->mChildren[i]->mName.data) != m_BoneMapping.end()) 
				allBones[m_BoneMapping[BoneName]].children.push_back(&(allBones[m_BoneMapping[node->mChildren[i]->mName.data]]));
		}
	}
	for (int i = 0; i < node->mNumChildren; i++)
	{
		BuildBoneHierarchy(node->mChildren[i]);
	}
}



Mesh Model::processMesh(aiMesh* mesh, const aiScene* scene)
{
	// Data to fill
	std::vector<Vertex> vertices;
	std::vector<GLuint> indices;
	std::vector<Texture> textures;
	
	// Walk through each of the mesh's vertices
	for(GLuint i = 0; i < mesh->mNumVertices; i++)
	{
		Vertex vertex;
		glm::vec3 vector; // We declare a placeholder std::vector since assimp uses its own std::vector class that doesn't directly convert to glm's vec3 class so we transfer the data to this placeholder glm::vec3 first.
		// Positions
		vector.x = mesh->mVertices[i].x;
		vector.y = mesh->mVertices[i].y;
		vector.z = mesh->mVertices[i].z;
		vertex.Position = vector;

		//Check bounds
		for (int j = 0; j < 3; j++) {
			if (vector[j] < minBounds[j])
				minBounds[j] = vector[j];
			if (vector[j] > maxBounds[j])
				maxBounds[j] = vector[j];
		}
		// Normals
		if (mesh->mNormals != nullptr)
		{
			vector.x = mesh->mNormals[i].x;
			vector.y = mesh->mNormals[i].y;
			vector.z = mesh->mNormals[i].z;
			vertex.Normal = vector;
		}
		else
		{
			vertex.Normal = glm::vec3(1);
		}

		// Texture Coordinates
		if(mesh->mTextureCoords[0]) // Does the mesh contain texture coordinates?
		{
			glm::vec2 vec;
			// A vertex can contain up to 8 different texture coordinates. We thus make the assumption that we won't 
			// use models where a vertex can have multiple texture coordinates so we always take the first set (0).
			vec.x = mesh->mTextureCoords[0][i].x; 
			vec.y = mesh->mTextureCoords[0][i].y;
			vertex.TexCoords = vec;
		}
		else
			vertex.TexCoords = glm::vec2(0.0f, 0.0f);
			
		if (mesh->mTangents != NULL) {
			// Tangent
			vector.x = mesh->mTangents[i].x;
			vector.y = mesh->mTangents[i].y;
			vector.z = mesh->mTangents[i].z;
			vertex.Tangent = vector;
			// Bitangent
			vector.x = mesh->mBitangents[i].x;
			vector.y = mesh->mBitangents[i].y;
			vector.z = mesh->mBitangents[i].z;
			vertex.Bitangent = vector;
		}
		else
		{
			vertex.Tangent = glm::vec3(0.0f, 0.0f, 0.0f);				
			vertex.Bitangent = glm::vec3(0.0f, 0.0f, 0.0f);
		}
		vertices.push_back(vertex);
	}

	//Load any bones present in mesh
	for (unsigned int i = 0 ; i < mesh->mNumBones ; i++) {                
		unsigned int BoneIndex = 0;        
		string BoneName(mesh->mBones[i]->mName.data);

		if (m_BoneMapping.find(BoneName) == m_BoneMapping.end()) {
			// Allocate an index for a new bone
			BoneIndex = allBones.size();
			numBones++;           
			Bone bi;			
			bi.name = BoneName;
			allBones.push_back(bi);
			for (int j =0; j < 4; j++)
				for (int k =0; k < 4; k++)	
					allBones[allBones.size() -1 ].BoneOffset[j][k] = mesh->mBones[i]->mOffsetMatrix[k][j];            
			
			
			m_BoneMapping[BoneName] = BoneIndex;
		}
		else {
			BoneIndex = m_BoneMapping[BoneName];
		}                      
		
		for (unsigned int j = 0 ; j < mesh->mBones[i]->mNumWeights ; j++) {
			unsigned int VertexID = mesh->mBones[i]->mWeights[j].mVertexId;// - numVerts;
			float Weight  = mesh->mBones[i]->mWeights[j].mWeight;
			vertices[VertexID].AddBoneData(BoneIndex, Weight);
		}
	}  

	// Now wak through each of the mesh's faces (a face is a mesh its triangle) and retrieve the corresponding vertex indices.
	for(GLuint i = 0; i < mesh->mNumFaces; i++)
	{
		aiFace face = mesh->mFaces[i];
		// Retrieve all indices of the face and store them in the indices std::vector
		for(GLuint j = 0; j < face.mNumIndices; j++)
			indices.push_back(face.mIndices[j]);
	}
	// Process materials
	if(mesh->mMaterialIndex >= 0)
	{
		aiMaterial* material = scene->mMaterials[mesh->mMaterialIndex];
		// We assume a convention for sampler names in the shaders. Each diffuse texture should be named
		// as 'texture_diffuseN' where N is a sequential number ranging from 1 to MAX_SAMPLER_NUMBER. 
		// Same applies to other texture as the following list summarizes:
		// Diffuse: texture_diffuseN
		// Specular: texture_specularN
		// Normal: texture_normalN

		// 1. Diffuse maps
		std::vector<Texture> diffuseMaps = this->loadMaterialTextures(material, aiTextureType_DIFFUSE, "texture_diffuse");
		textures.insert(textures.end(), diffuseMaps.begin(), diffuseMaps.end());
		// 2. Specular maps
		std::vector<Texture> specularMaps = this->loadMaterialTextures(material, aiTextureType_SPECULAR, "texture_specular");
		textures.insert(textures.end(), specularMaps.begin(), specularMaps.end());
		// 3. Normal maps
		vector<Texture> normalMaps = this->loadMaterialTextures(material, aiTextureType_HEIGHT, "texture_normal");
		textures.insert(textures.end(), normalMaps.begin(), normalMaps.end());
		// 4. Height maps
		vector<Texture> heightMaps = this->loadMaterialTextures(material, aiTextureType_AMBIENT, "texture_height");
		textures.insert(textures.end(), heightMaps.begin(), heightMaps.end());
	}
		
	numVerts += vertices.size();
	// Return a mesh object created from the extracted mesh data
	return Mesh(vertices, indices, textures);
}

// Checks all material textures of a given type and loads the textures if they're not loaded yet.
// The required info is returned as a Texture struct.
std::vector<Texture> Model::loadMaterialTextures(aiMaterial* mat, aiTextureType type, std::string typeName)
{
	std::vector<Texture> textures;
	for(GLuint i = 0; i < mat->GetTextureCount(type); i++)
	{
		aiString str;
		mat->GetTexture(type, i, &str);
		// Check if texture was loaded before and if so, continue to next iteration: skip loading a new texture
		GLboolean skip = false;
		for(GLuint j = 0; j < textures_loaded.size(); j++)
		{
			if(textures_loaded[j].path == str)
			{
				textures.push_back(textures_loaded[j]);
				skip = true; // A texture with the same filepath has already been loaded, continue to next one. (optimization)
				break;
			}
		}
		if(!skip)
		{   // If texture hasn't been loaded already, load it
			Texture texture;
			texture.id = TextureUtils::loadTextureFromFile((GLchar*)(this->directory + '/' + std::string(str.C_Str())).c_str());
			texture.type = typeName;
			texture.path = str;
			textures.push_back(texture);
			this->textures_loaded.push_back(texture);  // Store it as texture loaded for entire model, to ensure we won't unnecesery load duplicate textures.
		}
	}
	return textures;
}

GLint Model::TextureFromFile(const char* path, std::string directory, bool gamma)
{
	//Generate texture ID and load texture data 
	std::string filename = std::string(path);
	filename = directory + '/' + filename;
	GLuint textureID;
	glGenTextures(1, &textureID);
	int width,height;

	ILuint imageID;
	ilGenImages(1, &imageID);
	ilBindImage(imageID);
	ilEnable(IL_ORIGIN_SET);
	ilOriginFunc(IL_ORIGIN_UPPER_LEFT/*IL_ORIGIN_LOWER_LEFT*/);

	if (ilLoadImage((ILstring)path)) {
		// Assign texture to ID
		glBindTexture(GL_TEXTURE_2D, textureID);
		glTexImage2D(GL_TEXTURE_2D, 0, gamma ? GL_SRGB : GL_RGBA, ilGetInteger(IL_IMAGE_WIDTH), ilGetInteger(IL_IMAGE_HEIGHT), 0, GL_RGB, GL_UNSIGNED_BYTE, ilGetData());
		glGenerateMipmap(GL_TEXTURE_2D);	

		// Parameters
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glBindTexture(GL_TEXTURE_2D, 0);
		return textureID;
	} else {
		return -1;
	}
}