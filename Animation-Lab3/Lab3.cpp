// OGLProject.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

// GLEW
#define GLEW_STATIC
#include <GL/glew.h>

// GLFW
#include <glfw3.h>

//GL Maths
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

// Include AntTweakBar
//#include <AntTweakBar.h>

//Local Classes
#include "model.h"
#include "ShaderManager.hpp"
#include "Camera.h"

#include "CubeMap.hpp"

#include "Lighting.h"

#include <sstream> // for ostringstream

#include "DebugOutput.h"

static const int MAX_BONES = 100;

//Target Framerate (will not exceed)
const int TARGET_FPS = 60;

//Handles Program Initialisation 
bool glInit();

//Camera and Input Variables
Camera camera(glm::vec3(0.0f,0.0f, 100.0f));
bool grabbing;
bool debug = false;

bool keys[1024];
GLfloat lastX = 400, lastY = 300;
bool firstMouse = true;


// Function prototypes
//GLFW Input Callbacks
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode);
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);
void mouse_callback(GLFWwindow* window, double xpos, double ypos);
void mouse_button_callback(GLFWwindow* window, int button, int action, int mods);

void handleInput();
void drawScene(glm::mat4 v, glm::mat4 p);

//Window For OpenGL App
GLFWwindow* window;
// Window dimensions
const GLuint WIDTH = 800, HEIGHT = 600;
//Clipping Planes
float near = 0.1f;
float far = 10000.0f;

//For time-variant operations
GLfloat deltaTime = 0.0f;
GLfloat lastFrame = 0.0f;
GLfloat animationTime = 0.0f;
GLfloat t = 0.0;
GLint nbFrames = 0;

Model bob;
Model hand;

Shader *shader, *skinningShader;
CubeMap cubeMap;

int _tmain(int argc, _TCHAR* argv[])
{
		//Initialise OpenGL
	if (!glInit()) {
		std::cout << "Failed to initialise OpenGL";
		std::cin;
		return -1;
	}

	// Game loop
	while (!glfwWindowShouldClose(window))
	{
		
		// Set frame time
		GLfloat currentFrame = glfwGetTime();
		deltaTime = currentFrame - lastFrame;
		//Should lock framerate at target framerate
		if(deltaTime < (1.0f/TARGET_FPS))
			continue;
		lastFrame = currentFrame;
	
		nbFrames++;
		//Output frame time to console
		if ( currentFrame - lastFrame >= 1.0 ){ // If last prinf() was more than 1 sec ago

			// printf and reset timer

			printf("%f ms/frame\n", 1000.0/double(nbFrames));

			nbFrames = 0;

			lastFrame += 1.0;
		}

		// Check if any events have been activiated (key pressed, mouse moved etc.) and call corresponding response functions
		glfwPollEvents();
		handleInput();

		//Set BG Color & Clear the colorbuffer
		glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		if (grabbing)
		{
			animationTime += deltaTime;
		}
		else
		{
			animationTime -= deltaTime;

			if (animationTime <= 0.0f)
			{
				animationTime = 0.0f;
			}
		}

		// Create camera transformation
		glm::mat4 view;
		view = camera.GetViewMatrix();
		
		//Create Projection Matrix
		glm::mat4 projection;	
		projection = glm::perspective(camera.Zoom, (float)WIDTH/(float)HEIGHT, near, far);


		//Draw scene
		drawScene(view, projection);

		// Swap the screen buffers
		glfwSwapBuffers(window);
		t += deltaTime;
	}
		// Terminate GLFW, clearing any resources allocated by GLFW.
	glfwTerminate();
	return 0;
}


void drawScene(glm::mat4 v, glm::mat4 p)
{
	float time = glfwGetTime();

	time /= 2.0f;
	hand.getShader()->enableShader();
	hand.getShader()->setUniformVector3fv("camPos", camera.getPosition());
	Lighting::SetupLights(skinningShader);
	
	if (debug)
	{
		std::vector<glm::mat4> trans;
		hand.BoneTransform(animationTime, trans);
		hand.DrawJoints(v, p);
		hand.DrawBones(v, p);
	}
	else
		hand.Draw(v, p, animationTime);
	
	//Draw CubeMap
	cubeMap.getShader()->enableShader();
	cubeMap.getShader()->setUniformMatrix4fv("projectionMat", p);
	cubeMap.getShader()->setUniformMatrix4fv("viewMat", v);
	cubeMap.drawSkyBox();
	cubeMap.getShader()->disableShader();
}


bool glInit() {
	std::cout << "Starting GLFW context, OpenGL 3.3" << std::endl;
	
	//Initialise DevIL image loader
	ilInit();

	// Init GLFW
	if (!glfwInit())
	{
		std::cout << "ERROR: Could not initialise GLFW...";
		return false;
	}

	// Set all the required options for GLFW
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
	glfwWindowHint(GLFW_SAMPLES, 4);

	// Create a GLFWwindow object that we can use for GLFW's functions
	window = glfwCreateWindow(WIDTH, HEIGHT, "Animation Assignment 3", nullptr, nullptr);
	if (!window)
	{
		glfwTerminate();
		std::cout << "ERROR: Could not create winodw...";
		return false;
	}

	glfwMakeContextCurrent(window);

	// Set the required callback functions
	glfwSetKeyCallback(window, key_callback);
	glfwSetCursorPosCallback(window, mouse_callback);
	glfwSetScrollCallback(window, scroll_callback);
	glfwSetMouseButtonCallback(window, mouse_button_callback);
	glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);

	//Grab and hide mouse cursor
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	//glfwSetCursor(window, cursor);

	// Initialize GLEW
	glewExperimental = GL_TRUE; // Needed for core profile
	if (glewInit() != GLEW_OK) {
		fprintf(stderr, "Failed to initialize GLEW\n");
		return false;
	}

	// Define the viewport dimensions
	glViewport(0, 0, WIDTH, HEIGHT);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS); 

	//Set up lighting in the scene
	Lighting::addLight(PointLight(glm::vec3(10.0f, 60.0f, 70.0f)));
	
	skinningShader = ShaderManager::loadShader("skinning");
	
	hand.load("../models/hand/hand.DAE");
	hand.setShader(skinningShader);
	hand.scale(glm::vec3(10));

	bob.load("../models/bob/boblampclean.md5mesh");
	bob.setShader(skinningShader);
	bob.rotateBy(glm::vec3(-1.57f, 0.0f, 0.0f));
	bob.translate(glm::vec3(0.0f, 0.0f, -25.0f));
	bob.scale(glm::vec3(0.5f));
	//cubemap initialisation
	cubeMap.loadCubeMap("../textures/cubemaps/ocean_sky/");
	cubeMap.setShader(ShaderManager::loadShader("skybox")); //TODO - This could definitely be hard-coded...

	DebugOutput::Init();

	return true;
}

// Moves/alters the camera positions based on user input
void handleInput()
{
	if(keys[GLFW_KEY_LEFT_SHIFT])
	{
		if(keys[GLFW_KEY_D])
		{
			debug = !debug;
			keys[GLFW_KEY_D] = false;
		}
	}
	// Camera controls
	if(keys[GLFW_KEY_W])
	{
		camera.ProcessKeyboard(FORWARD, deltaTime);
	}
	if(keys[GLFW_KEY_S])
	{
		camera.ProcessKeyboard(BACKWARD, deltaTime);
	}
	if(keys[GLFW_KEY_A])
	{
		camera.ProcessKeyboard(LEFT, deltaTime);

	}
	if(keys[GLFW_KEY_D])
	{
		camera.ProcessKeyboard(RIGHT, deltaTime);
	}
	if(keys[GLFW_KEY_SPACE])
	{
		float thisLineExistsOnlyforABreakPoint = 0.0f;
	}
}

//Is called when a mouse button is pushed
void mouse_button_callback(GLFWwindow* window, int button, int action, int mods)
{
	if (button == GLFW_MOUSE_BUTTON_LEFT){
		switch (action)
		{
		case GLFW_RELEASE:
			grabbing = false;
			break;
		case GLFW_PRESS:
			grabbing = true;
			break;
		default:
			//what other actions are there for a mouse button?
			break;
		}
	}	 
}

// Is called whenever a key is pressed/released via GLFW
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode)
{
	//Had to put in this guard because media keys would crash the application
	if (key > 1024 || key < 0)
		return;

	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GL_TRUE);

	if(action == GLFW_PRESS)
		keys[key] = true;
	else if(action == GLFW_RELEASE)
		keys[key] = false;
}

void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
	if(firstMouse)
	{
		lastX = (float) xpos;
		lastY = (float) ypos;
		firstMouse = false;
	}

	GLfloat xoffset = (float) xpos - lastX;
	GLfloat yoffset = lastY - ypos;  // Reversed since y-coordinates go from bottom to left
	
	lastX = xpos;
	lastY = ypos;

	camera.ProcessMouseMovement(xoffset, yoffset);
}	


void scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
{
	camera.ProcessMouseScroll(yoffset * 0.1);
}