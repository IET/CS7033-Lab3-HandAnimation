#include "Lighting.h"

std::vector<PointLight>			Lighting::pointLights;
std::vector<SpotLight>			Lighting::spotLights;

DirectionalLight Lighting::directionalLight;

void Lighting::addLight(PointLight light)
{
	pointLights.push_back(light);
}

void Lighting::addLight(SpotLight light)
{
	spotLights.push_back(light);
}

void Lighting::SetDirectionalLight(DirectionalLight light)
{
	directionalLight = light;
}

void Lighting::SetupLights(Shader* s)
{
	//Attach directional light data
	s->setUniformVector3fv("directionalLight.Base.Color",		directionalLight.color);
	s->setUniform1f("directionalLight.Base.AmbientIntensity",	directionalLight.ambientIntensity);
	s->setUniform1f("directionalLight.Base.DiffuseIntensity",	directionalLight.diffuseIntensity);
	s->setUniformVector3fv("directionalLight.Direction",		glm::normalize(directionalLight.direction));

	//Attach point light data
	s->setUniform1i("numPointLights", pointLights.size());

	for (int i = 0; i < pointLights.size(); i++)
	{
		// Position
		std::ostringstream pointPosUniform;
		pointPosUniform << "pointLights[" << i << "].Position";
		s->setUniformVector3fv(pointPosUniform.str(), pointLights[i].position);

		// Color
		std::ostringstream pointColUniform;
		pointColUniform << "pointLights[" << i << "].Base.Color";
		s->setUniformVector3fv(pointColUniform.str(), pointLights[i].color);

		// Ambient Intensity
		std::ostringstream pointAmbIntUniform;
		pointAmbIntUniform << "pointLights[" << i << "].Base.AmbientIntensity";
		s->setUniform1f(pointAmbIntUniform.str(), pointLights[i].ambientIntensity);
		
		// Diffuse intensity
		std::ostringstream pointDiffIntUniform;
		pointDiffIntUniform << "pointLights[" << i << "].Base.DiffuseIntensity";
		s->setUniform1f(pointDiffIntUniform.str(), pointLights[i].diffuseIntensity);

		// Constant Attenuation
		std::ostringstream pointConstAttenUniform;
		pointConstAttenUniform << "pointLights[" << i << "].Atten.Constant";
		s->setUniform1f(pointConstAttenUniform.str(), pointLights[i].attenuation.constant);

		// Linear Attenuation
		std::ostringstream pointLinearAttenUniform;
		pointLinearAttenUniform << "pointLights[" << i << "].Atten.Linear";
		s->setUniform1f(pointLinearAttenUniform.str(), pointLights[i].attenuation.linear);

		// Exponential Attenuation
		std::ostringstream pointExpAttenUniform;
		pointExpAttenUniform << "pointLights[" << i << "].Atten.Exp";
		s->setUniform1f(pointExpAttenUniform.str(), pointLights[i].attenuation.exp);
	}

	//Attach spot light data
	s->setUniform1i("numSpotLights", spotLights.size());

	for (int i = 0; i < spotLights.size(); i++)
	{
		// Position
		std::ostringstream spotPosUniform;
		spotPosUniform << "spotLights[" << i << "].Position";
		s->setUniformVector3fv(spotPosUniform.str(), spotLights[i].position);

		// Direction
		std::ostringstream spotDirUniform;
		spotDirUniform << "spotLights[" << i << "].Direction";
		s->setUniformVector3fv(spotDirUniform.str(), spotLights[i].direction);

		// Color
		std::ostringstream spotColUniform;
		spotColUniform << "spotLights[" << i << "].Base.Color";
		s->setUniformVector3fv(spotColUniform.str(), spotLights[i].color);

		// Ambient Intensity
		std::ostringstream spotAmbIntUniform;
		spotAmbIntUniform << "spotLights[" << i << "].AmbientInensity";
		s->setUniform1f(spotAmbIntUniform.str(), spotLights[i].ambientIntensity);
		
		// Cutoff
		std::ostringstream spotCutoffUniform;
		spotCutoffUniform << "spotLights[" << i << "].Cutoff";
		s->setUniform1f(spotCutoffUniform.str(), spotLights[i].cutoff);

		// Diffuse intensity
		std::ostringstream spotDiffIntUniform;
		spotDiffIntUniform << "spotLights[" << i << "].DiffuseIntensity";
		s->setUniform1f(spotDiffIntUniform.str(), spotLights[i].diffuseIntensity);

		// Constant Attenuation
		std::ostringstream spotConstAttenUniform;
		spotConstAttenUniform << "spotLights[" << i << "].Atten.Constant";
		s->setUniform1f(spotConstAttenUniform.str(), spotLights[i].attenuation.constant);

		// Linear Attenuation
		std::ostringstream spotLinearAttenUniform;
		spotLinearAttenUniform << "spotLights[" << i << "].Atten.Linear";
		s->setUniform1f(spotLinearAttenUniform.str(), spotLights[i].attenuation.linear);

		// Exponential Attenuation
		std::ostringstream spotExpAttenUniform;
		spotExpAttenUniform << "spotLights[" << i << "].Atten.Exp";
		s->setUniform1f(spotExpAttenUniform.str(), spotLights[i].attenuation.exp);
	}
}


