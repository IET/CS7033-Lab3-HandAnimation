//Courtesy of http://learnopengl.com/
#pragma once
// Std. Includes
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <vector>
using namespace std;
// GL Includes
#include <GL/glew.h> // Contains all the necessery OpenGL includes
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <assimp/Importer.hpp>

#include <stdlib.h> //rand
#include <time.h> 

#include "Shader.hpp"

	
//Data for skeletal animation
#define NUM_BONES_PER_VEREX 6	//Maximum no. of bones that can affect one vertex. Best kept low for banwidth reasons.
struct Bone
{
	glm::mat4 BoneOffset;
	glm::mat4 FinalTransformation;        
	glm::mat4 localTransform;

	std::string name;
	
	Bone()
	{
		BoneOffset = glm::mat4(0);
		FinalTransformation = glm::mat4(0);            
	}

	std::vector<Bone*> children;
};

//Per-Vertex Data
struct Vertex {
	// Position
	glm::vec3 Position;
	// Normal
	glm::vec3 Normal;
	// TexCoords
	glm::vec2 TexCoords;
	// Tangent
	glm::vec3 Tangent;
	// Bitangent
	glm::vec3 Bitangent;

	//Bone Data
	unsigned int BoneIDs[NUM_BONES_PER_VEREX];
	float BoneWeights[NUM_BONES_PER_VEREX];
	
	Vertex()
	{
		for (unsigned int i = 0 ; i < sizeof(BoneIDs)/sizeof(BoneIDs[0]) ; i++) {
			BoneIDs[i] = 0;
			BoneWeights[i] = 0.0f;
		}
	}

	void AddBoneData(unsigned int BoneID, float Weight)
	{
		for (unsigned int i = 0 ; i < sizeof(BoneIDs)/sizeof(BoneIDs[0]) ; i++) {
			if (BoneWeights[i] == 0.0) {
				BoneIDs[i]     = BoneID;
				BoneWeights[i] = Weight;
				return;
			}        
		}
		// should never get here - more bones than we have space for
		assert(0);
	}

};

struct Texture {
	GLuint id;
	string type;
	aiString path;
};

class Mesh {
public:
	/*  Mesh Data  */
	vector<Vertex> vertices;
	vector<GLuint> indices;
	vector<Texture> textures;
	GLuint VAO;


	/*  Functions  */
	// Constructors
	Mesh(vector<Vertex> vertices, vector<GLuint> indices, vector<Texture> textures);

	// Render the mesh
	void Draw(Shader* shader);


	~Mesh();

private:
	bool		isInstanced;
	int			instances;

	/*  Functions    */
	// Initializes all the buffer objects/arrays
	void setupMesh();

protected:
	Mesh();	//TODO - Implement some kind of stub

	/*  Render data  */
	GLuint VBO, EBO;
};



