#pragma once
#include <gl/glew.h>
#include <glm/glm.hpp>
#include <vector>
#include "Shader.hpp"

struct Attenuation
{
	float constant;
	float linear;
	float exp;
};

class Light {
public:
	glm::vec3 color;
	float ambientIntensity;
	float diffuseIntensity;
};

class DirectionalLight : public Light
{
public:
	glm::vec3 direction;

	DirectionalLight()
	{
		direction = glm::vec3(0);
	}
};

class PointLight : public Light
{
public:
	glm::vec3 position;
	Attenuation attenuation;
	
	PointLight()
	{
		position = glm::vec3(0);
		ambientIntensity = 50.0f;
		diffuseIntensity = 50.0f;

		attenuation.constant = 1.0f;
		attenuation.linear = 1.0f;
		attenuation.exp = 0.0f;
	}

	PointLight(glm::vec3 pos)
		:position(pos)
	{
		ambientIntensity = 50.0f;
		diffuseIntensity = 200.0f;
		color = glm::vec3(1.0f, 1.0f, 1.0f);
		attenuation.constant = 1.0f;
		attenuation.linear   = 1.0f;
		attenuation.exp		 = 0.1f;
	}

	PointLight(glm::vec3 pos, glm::vec3 col)
		:position(pos)
	{
		color = col;
		ambientIntensity = 50.0f;
		diffuseIntensity = 50.0f;

		attenuation.constant = 1.0f;
		attenuation.linear	 = 1.0f;
		attenuation.exp		 = 0.0f;
	}
};

class SpotLight : public PointLight
{
public:
	glm::vec3 direction;
	float cutoff;

	SpotLight()
	{
		direction = glm::vec3(0);
		cutoff = 0.0f;
	}
};

class Lighting
{
public:
	static void addLight(PointLight			light);
	static void addLight(SpotLight			light);
	static void addLight(DirectionalLight	light);

	static void SetDirectionalLight(DirectionalLight light);

	static void SetupLights(Shader* s);
private:
	static std::vector<PointLight>			pointLights;
	static std::vector<SpotLight>			spotLights;

	static DirectionalLight directionalLight;
};

