// GLEW
#define GLEW_STATIC
#include <GL/glew.h>
#include <IL\il.h>
#include <iostream>

static class TextureUtils {
public:
	static GLuint loadTextureFromFile(GLchar const * path)
	{
		//Make sure DevIL is initialised
		ilInit();

		//Generate texture ID and load texture data 
		GLuint textureID;
		glGenTextures(1, &textureID);
		int width, height;
		//unsigned char* image = SOIL_load_image(path, &width, &height, 0, SOIL_LOAD_RGB);
	
		ILuint imageID;
		ilGenImages(1, &imageID);
		ilBindImage(imageID);
		ilEnable(IL_ORIGIN_SET);
		ilOriginFunc(IL_ORIGIN_UPPER_LEFT/*IL_ORIGIN_LOWER_LEFT*/);

		if (ilLoadImage((ILstring)path))
		{
			ilConvertImage(IL_RGBA, IL_UNSIGNED_BYTE);

			// Assign texture to ID
			glBindTexture(GL_TEXTURE_2D, textureID);
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, ilGetInteger(IL_IMAGE_WIDTH), ilGetInteger(IL_IMAGE_HEIGHT), 0, GL_RGBA, GL_UNSIGNED_BYTE, ilGetData());
			glGenerateMipmap(GL_TEXTURE_2D);

			// Parameters
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glBindTexture(GL_TEXTURE_2D, 0);
			return textureID;
		}
		else 
		{
			std::cout << "Couldn't load texture: " << path << std::endl;
			return -1;
		}

	
	}
};