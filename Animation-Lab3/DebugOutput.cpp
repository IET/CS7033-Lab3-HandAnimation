#include "DebugOutput.h"
#include "ShaderManager.hpp"
#include "Shader.hpp"
#include <glm/glm.hpp>
#include <glm/gtx/norm.hpp>
#include <glm/gtc/matrix_transform.hpp>

DebugSphere DebugOutput::debugSphere;//.VAO;
//GLuint DebugOutput::debugSphere.VBO;
//float DebugOutput::debugSphere.sphereVertices[72];
DebugLine DebugOutput::debugLine;
Shader* DebugOutput::shader;

//TODO - Split this up because lines and spheres don't actually have that much in common

void DebugOutput::Init()
{
	DebugOutput::shader = ShaderManager::loadShader("debug");

	glGenVertexArrays(1, &DebugOutput::debugSphere.VAO);
	glGenBuffers(1, &DebugOutput::debugSphere.VBO);
	
	glBindVertexArray(DebugOutput::debugSphere.VAO);
	glBindBuffer(GL_ARRAY_BUFFER, DebugOutput::debugSphere.VBO);

	glBufferData(GL_ARRAY_BUFFER, sizeof(DebugOutput::debugSphere.sphereVertices), DebugOutput::debugSphere.sphereVertices, GL_STATIC_DRAW);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), nullptr);
	glEnableVertexAttribArray(0);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);


	glGenVertexArrays(1, &DebugOutput::debugLine.VAO);
	glGenBuffers(1, &DebugOutput::debugLine.VBO);
	
	glBindVertexArray(DebugOutput::debugLine.VAO);
	glBindBuffer(GL_ARRAY_BUFFER, DebugOutput::debugLine.VBO);

	glBufferData(GL_ARRAY_BUFFER, sizeof(DebugOutput::debugLine.vertices), DebugOutput::debugLine.vertices, GL_STATIC_DRAW);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), nullptr);
	glEnableVertexAttribArray(0);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
}

void DebugOutput::DrawSphere(glm::mat4 modelMatrix, glm::mat4 v, glm::mat4 p, glm::vec3 pos, float radius)
{
	float j = 0;
	//TODO - Scale the output rather than recalculating the shape of the sphere each frame
	for (int i =0; i < 70; i +=3)
	{
		j = 6.28f/71.0f * i;

		DebugOutput::debugSphere.sphereVertices[i]		= (cos(j) * radius);
		DebugOutput::debugSphere.sphereVertices[i+1]	= (sin(j) * radius);
		DebugOutput::debugSphere.sphereVertices[i+2]	= 0.0f;
	}
	DebugOutput::shader->enableShader();

	glBindVertexArray(DebugOutput::debugSphere.VAO);
	glBindBuffer(GL_ARRAY_BUFFER, DebugOutput::debugSphere.VBO);
	glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(DebugOutput::debugSphere.sphereVertices), &DebugOutput::debugSphere.sphereVertices);
	glBindBuffer(GL_ARRAY_BUFFER, 0);


	float gridResolution = 3.0f;
	DebugOutput::shader->setUniformVector4fv("col", glm::vec4(0.0f, 1.0f, 0.0f, 1.0f));
	DebugOutput::shader->setUniformMatrix4fv("projectionMat", p);
	DebugOutput::shader->setUniformMatrix4fv("viewMat", v);

	glm::mat4 model = modelMatrix;

	for (float i = 0.0f; i <  3.14f; i += 3.14f/gridResolution)
	{
		glm::rotate(model, i, glm::vec3(0,1,0));

		DebugOutput::shader->setUniformMatrix4fv("modelMat", model);

		glDrawArrays(GL_LINE_LOOP, 0, 24);
	}

	glm::mat4 rot90 = glm::rotate(model, 1.57f, glm::vec3(1,0,0));

	for (float i = 0.0f; i <  3.14f; i += 3.14f/gridResolution)
	{
		glm::mat4 rotMod = glm::rotate(rot90, i, glm::vec3(0,1,0));

		DebugOutput::shader->setUniformMatrix4fv("modelMat",  rotMod);

		glDrawArrays(GL_LINE_LOOP, 0, 24);
	}
	
	glBindVertexArray(0);

	DebugOutput::shader->disableShader();
}

//TODO - Allow an array of start-emnd points so you can generate only one draw call
void DebugOutput::DrawLine(glm::mat4 modelMatrix, glm::mat4 v, glm::mat4 p, glm::vec3 start, glm::vec3 end)
{
	DebugOutput::shader->enableShader();

	DebugOutput::debugLine.vertices[0] = start.x;
	DebugOutput::debugLine.vertices[1] = start.y;
	DebugOutput::debugLine.vertices[2] = start.z;

	DebugOutput::debugLine.vertices[3] = end.x;
	DebugOutput::debugLine.vertices[4] = end.y;
	DebugOutput::debugLine.vertices[5] = end.z;

	glBindVertexArray(DebugOutput::debugLine.VAO);
	glBindBuffer(GL_ARRAY_BUFFER, DebugOutput::debugLine.VBO);
	glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(DebugOutput::debugLine.vertices), &DebugOutput::debugLine.vertices);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	DebugOutput::shader->setUniformVector4fv("col", glm::vec4(0.0f, 0.0f, 1.0f, 1.0f));
	DebugOutput::shader->setUniformMatrix4fv("projectionMat", p);
	DebugOutput::shader->setUniformMatrix4fv("viewMat", v);
	DebugOutput::shader->setUniformMatrix4fv("modelMat", modelMatrix);

	glDrawArrays(GL_LINES, 0, 2);
	
	glBindVertexArray(0);
	DebugOutput::shader->disableShader();
}

//This approach may be more GPU friendly than the above
void DebugOutput::DrawLine(glm::mat4 modelMatrix, glm::mat4 v, glm::mat4 p, std::vector<glm::vec3> startPoints, std::vector<glm::vec3> endPoints)
{
	DebugOutput::shader->enableShader();

	int j = 0;
	for (int i=0; i < startPoints.size() && i < MAX_LINES; i++)
	{
		DebugOutput::debugLine.vertices[j]		= startPoints[i].x;
		DebugOutput::debugLine.vertices[j + 1]	= startPoints[i].y;
		DebugOutput::debugLine.vertices[j + 2]	= startPoints[i].z;

		DebugOutput::debugLine.vertices[j + 3]	= endPoints[i].x;
		DebugOutput::debugLine.vertices[j + 4]	= endPoints[i].y;
		DebugOutput::debugLine.vertices[j + 5]	= endPoints[i].z;

		j += 6;
	}

	glBindVertexArray(DebugOutput::debugLine.VAO);
	glBindBuffer(GL_ARRAY_BUFFER, DebugOutput::debugLine.VBO);
	glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(DebugOutput::debugLine.vertices), &DebugOutput::debugLine.vertices);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	DebugOutput::shader->setUniformVector4fv("col", glm::vec4(0.0f, 0.0f, 1.0f, 1.0f));
	DebugOutput::shader->setUniformMatrix4fv("projectionMat", p);
	DebugOutput::shader->setUniformMatrix4fv("viewMat", v);
	DebugOutput::shader->setUniformMatrix4fv("modelMat", modelMatrix);

	glDrawArrays(GL_LINES, 0, startPoints.size() > MAX_LINES ? MAX_LINES : startPoints.size());
	
	glBindVertexArray(0);
	DebugOutput::shader->disableShader();
}